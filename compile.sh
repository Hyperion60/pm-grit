#!/bin/bash


mkdir -p build

mkdir -p build/TP1
gcc -Wall -Wextra TP1/print_signal.c -o build/TP1/print_signal
gcc -Wall -Wextra TP1/catch_signal.c -o build/TP1/catch_signal
gcc -Wall -Wextra TP1/catch_print_signal.c -o build/TP1/catch_print_signal
gcc -Wall -Wextra TP1/catch_sigusr_signal.c -o build/TP1/catch_sigusr_signal
